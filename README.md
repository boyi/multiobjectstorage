This software is used for easy accessing multiple Objectstores and grant basic operations to the object stores.
It also helps to mange the data.

How to use:

build your Image with the Dockerfile (care for the nametag)

create the secrets

you need 3 secrets

becker_key: here you add your ssl key

becker_cert: here you add your ssl cert

becker_keys: here you add following string:
module.exports = {
        google: {
            clientID: '',
            clientSecret: ''
        },
        mongodb: {
            dbURI: 'mongodb://1.1.1.:27017'
        },
        session: {
            cookieKey: ''
        },
        mysecret: {
          key : ''
        }
};

bring it up with "docker deploy"



now you can acces the mainpage via https

there you can login via google, this create an account.

in your account you can create your a jwt-token for the api, everytime your visit this site the token is new created.
the OLD token is then useless.
and you can add your objectStorages.

for this you need to be carefull
you can type any name,
the type must be s3 or google
and the time must be a numer in seconds
for the json key look the next section

S3
{"endPoint": "ip",
    "port": 9000,
    "secure": false,
    "accessKey": "",
    "secretKey": "" }

GOOGLE
for finding this data maybe this link helps: https://www.npmjs.com/package/@google-cloud/storage
{
"projectId": ""
     , "location": "US"               
     , "storageClass" : "Coldline"
     , "cred" :
{
  "type": "",
  "project_id": "",
  "private_key_id": "",
  "private_key": " ",
  "client_email": "",
  "client_id": "",
  "auth_uri": ",
  "token_uri": "",
  "auth_provider_x509_cert_url": "",
  "client_x509_cert_url": ""
}
}

you can add as many storages of one typ as you like

API functions

to use the api's you need your jwt-token, add it in the header:
key: Authorization , value: "bearer jwt-token"

put /multiobjstor/v1.0.0
upload a data
needed Parameter in the body:
bucket : in which bucket i should be upload, if it doesn't exit the bucket will be created
data : the file itself
obj_store  : in which objectstor it should be uploaded
optional Paramter
location : location for the google api for creating a new bucket if not give the standart will be used from the settings
storageClass : storageClass for the google api for creating a new bucket if not give the standart will be used from the settings
data_id : under this id you can acces your data through this api again. if not given it will autocreate one, you will receive as a answer

get /multiobjstor/alldata/v1.0.0
you got al information about all your data that you stored via this API

get /multiobjstor/metadata/v1.0.0
you got all saved metadatasabout one files
needed paramter in url
data_id

get /multiobjstor/v1.0.0
you got a link to download the file
needed paramter in url
data_id
optional paramter in url
expiresinS : for how long you can use the link, if nothing give the standart will be used

get /delete/v1.0.0
you delete a file
needed paramter in url
data_id




License
Code
The code is distrubuted under the GNU AGPLv3 license.
