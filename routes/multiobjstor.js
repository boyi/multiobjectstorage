const router = require('express').Router();
const passport = require('passport');
const request = require('request-promise');
const randomstring = require('randomstring');


const User = require('../models/user-model');



router.delete('/v1.0.0' ,passport.authenticate('jwt', { session: false }), (req, res) => {

  // Paramter
  if (req.query.data_id   )
  {

    //get storage typ for further accessing#
    var dataNum = getDataforData(req.user, req.query.data_id);
    var storageNum = getDataforStorage(req.user, req.user.data[dataNum].obj_store );
    var storTyp = req.user.objectStorag[storageNum].typ;
    const random = '../functions/' + storTyp;
    var storagefunctions = require(random);
  storagefunctions.delete(req.user.data[dataNum].data_name_obj , req.user.data[dataNum].bucket , storageNum, req.user )
    .then( result => {
      var deletedata = {
        _id :req.user.data[dataNum]._id
        ,data_id: req.query.data_id
        ,data_name_obj: req.user.data[dataNum].data_name_obj
        ,obj_store: req.user.data[dataNum].obj_store
        ,bucket: req.user.data[dataNum].bucket
      }
     User.update( {googleId: req.user.googleId},
        { $pullAll: {data: [deletedata] } }, function(err ) {
          if (err) { res.status(400); res.send(err)}
          console.log(err);
      console.log(result);
      res.send(result);
    })
    })
    .catch( err=> {
      console.log(err);
      res.status(400);
      res.send(err);
    });

}
else {

  res.status(400);
  res.send('wrong or missing paramter')
  console.log('wrong or missing paramter'.red);
}
});
router.get('/v1.0.0' ,passport.authenticate('jwt', { session: false }), (req, res) => {
  // get the elemet or the header or both, or all elemts name
  // Paramter
  if (req.query.data_id   )
  // optional Paramter
  // req.query.expiresinS

  { var dataNum = getDataforData(req.user, req.query.data_id);
    var storageNum = getDataforStorage(req.user, req.user.data[dataNum].obj_store );

    //get storage typ for further accessing#

    var storTyp = req.user.objectStorag[storageNum].typ;
    //  set standart values for optional parameter for google only
    var expiresinS = parseInt (req.query.expiresinS || req.user.objectStorag[storageNum].expiresinS);
    const random = '../functions/' + storTyp;
    var storagefunctions = require(random);
    storagefunctions.download(req.user.data[dataNum].data_name_obj , req.user.data[dataNum].bucket  , expiresinS , storageNum, req.user)
      .then( result => {
        console.log(result);
        res.send(result);
      })
      .catch( err=> {
        console.log(err);
      });
    }
    else {
      res.status(400);
      res.send('wrong or missing paramter')
      console.log('wrong or missing paramter'.red);
    }
  });

router.get('/metadata/v1.0.0' ,passport.authenticate('jwt', { session: false }), (req, res) => {
  // get the elemet or the header or both, or all elemts name
    // Paramter
    if (req.query.data_id  )
    // optional Paramter
    {

      var dataNum = getDataforData(req.user, req.query.data_id);
      var storageNum = getDataforStorage(req.user, req.user.data[dataNum].obj_store );
      //get storage typ for further accessing#

      var storTyp = req.user.objectStorag[storageNum].typ;
      const random = '../functions/' + storTyp;
      var storagefunctions = require(random);
    storagefunctions.getMetadata(req.user.data[dataNum].data_name_obj , req.user.data[dataNum].bucket , storageNum, req.user)
      .then( result => {
        console.log(result);
        res.send(result);
      })
      .catch( err=> {
        console.log(err);
      });
    }
    else {
      console.log(req);
      res.status(400);
      res.send('wrong or missing paramter')
      console.log('wrong or missing paramter'.red);
    }

});

router.get('/alldata/v1.0.0' ,passport.authenticate('jwt', { session: false }), (req, res) => {

res.send(req.user.data)

})

router.put('/v1.0.0' ,passport.authenticate('jwt', { session: false }), (req, res) => {

  // Paramter
  if (req.body.bucket && req.body.data && req.body.obj_store  )
  // optional Paramter
  // req.body.location
  // req.body.storageClass
  //req.body.data_id
  {
    //get the Number for further accessing the array of Storages
    var storageNum = getDataforStorage(req.user, req.body.obj_store )
    //get storage typ for further accessing#

    var storTyp = req.user.objectStorag[storageNum].typ
    //  set standart values for optional parameter for google only
    if (storTyp == "google"){
      var location = req.body.location || req.user.objectStorag[storageNum].accesGO.location;
      var storageClass = req.body.storageClass || req.user.objectStorag[storageNum].accesGO.storageClass;
    };
    const random = '../functions/' + storTyp;
    var storagefunctions = require(random);
    checkdataIDorcreate(req.user, req.body.data_id)
      .then( data_id => {

        storagefunctions.upload(req.body.data, req.body.bucket, location, storageClass, storageNum,req.user )
        .then (result => {
          console.log(result);
          var newData = {
            data_id: data_id
            ,data_name_obj: req.body.data.path.substr(req.body.data.path.lastIndexOf("\\" )+1)
            ,obj_store: req.body.obj_store
            ,bucket: req.body.bucket
          }
          User.update({googleId: req.user.googleId}, {
            $push: {data: newData }
          //  user.data[newposition].data_name_obj: data.path.substr(data.path.lastIndexOf("\\" )+1) ;
          //  user.data[newposition].obj_store:
          //  user.data[newposition].bucket:

        }, function(err, numberAffected, rawResponse) {
          if (err) { res.status(400); res.send(err)}
          res.status(200);
          res.send ("upload successfull with id"+ data_id);  }) })
        .catch (err => {
          console.log(err);
          res.status(400);
          res.send(err);
        })
      })
      .catch( err => {
        console.log(err);
        res.status(400);
        res.send(err);
      })


  }
  else {

    res.status(400);
    res.send('wrong or missing paramter')
    console.log('wrong or missing paramter'.red);
  }


});
function getDataforStorage( user, storageName){
  for(var i = 0; i < user.objectStorag.length; i++){
    if (user.objectStorag[i].name == storageName)
    {return i};
  }

}
function getDataforData(user, data_id) {
  for(var i = 0; i < user.data.length; i++){
    if (user.data[i].data_id == data_id)
    {return i};
  }
}

function checkdataIDorcreate(user , data_id ){
  return new Promise(function(resolve,reject){
    var correctIDnotfounded = true;
    var createnonewID = false;
    if (data_id){
      var createnonewID = true
    }

    while ( correctIDnotfounded )
    {
      if (!createnonewID){

        data_id = randomstring.generate({length: 30});
      }

        for ( var i = 0; i <= user.data.length; i++){

            if ( i == user.data.length )
            {
              correctIDnotfounded = false ;
              resolve( data_id);
              break;
            }
            if (user.data[i].data_id == data_id){
              if ( createnonewID )
              {
                 var err = "id already in use";
                 correctIDnotfounded = false;
                 reject(err);
                 break;
               }
              else { break ;}
            }
        }
    }
  })
}

module.exports = router;
