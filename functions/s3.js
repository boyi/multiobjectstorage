const Minio = require('minio');
const router = require('express').Router();
const request = require('request-promise');

class s3{

  upload( data, bucket, valuewenotneed, valuewenotneed2, storageNum, user) {
    return new Promise(function(resolve,reject){

      var minioClient = new Minio.Client(
        user.objectStorag[storageNum].accesS3
      );

      minioClient.bucketExists(bucket, function(err, exists) {
        if (err || exists == false) {
          minioClient.makeBucket(bucket , function(err) {
            if (err) {reject(err)};
            console.log('Bucket ' +bucket+ ' created successfully ');
            uploadminio(data, bucket, minioClient)
              .then( res => { resolve(res)})
              .catch( err =>{ reject(err)});
            });
        }
        if (exists) {
          console.log('Bucket exists.')
          uploadminio(data, bucket, minioClient)
            .then( res => { resolve(res)})
            .catch( err =>{ reject(err)});
        };
      });
    });
  };

  download(fileName , bucket , expiresinS , storageNum, user) {
    return new Promise(function(resolve,reject){
      var minioClient = new Minio.Client(
        user.objectStorag[storageNum].accesS3
      );
       var size = 0
       minioClient.presignedGetObject(bucket, fileName, expiresinS, function(err, presignedUrl) {
         if (err) {reject(err);}
         else {resolve(presignedUrl);}
       })
     });
   };

   getMetadata(fileName, bucket , storageNum, user) {
     return new Promise(function(resolve,reject){
       var minioClient = new Minio.Client(
         user.objectStorag[storageNum].accesS3
       );
       var size = 0

       minioClient.statObject(bucket, fileName,  function(err, stat) {
         if (err) {reject(err)}
         else {
           resolve(stat);
         }
       })
     });
   };

   delete(fileName, bucket, storageNum, user) {
     return new Promise(function(resolve,reject){
       var minioClient = new Minio.Client(
         user.objectStorag[storageNum].accesS3
       );


       minioClient.removeObject(bucket, fileName,  function(err) {
         if (err) {reject(err);}
          resolve("object succesfull removed");
       })
     });
   };
}

function uploadminio(data, bucket, minioClient) {
  return new Promise(function(resolve,reject){

  minioClient.putObject(bucket, data.path.substr(data.path.lastIndexOf("\\" )+1), data, function(err, etag) {
    if (err) {reject(err)}
    else {console.log('File uploaded successfully.')
      resolve("succes")};
    });
  });
};

module.exports=new s3;
