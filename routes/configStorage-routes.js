const router = require('express').Router();
const config = require('config');
const User = require('../models/user-model');
const keys = require(config.keypfad);



const authCheck = (req, res, next) => {
    if(!req.user){
        res.redirect('/auth/login');
    } else {
        next();
    }
};

router.get('/', authCheck, (req, res) => {
//let template = ejs.compile(str, options);
    res.render('configStorage', { user: req.user  });





});

router.post("/adddata",authCheck,  (req, res) => {




  var newData =
  {
      name: req.body.name
      ,typ: req.body.typ.toLowerCase()
      ,expiresinS: req.body.expiresinS
      ,accesS3: {}
      ,accesGO: {}
  }

  objectData = JSON.parse(req.body.data);

  if ( newData.typ == "s3")
  {

    newData.accesS3 = objectData;
  }
  else if (newData.typ == "google") {

    newData.accesGO = objectData;
  }
console.log(newData);

  User.update({googleId: req.user.googleId}, {
    $push: {objectStorag: newData }
  //  user.data[newposition].data_name_obj: data.path.substr(data.path.lastIndexOf("\\" )+1) ;
  //  user.data[newposition].obj_store:
  //  user.data[newposition].bucket:

}, function(err, numberAffected, rawResponse) {
  if (err) { res.status(400);console.log(err); res.send(err)}

 console.log("successfull added an objectStorag");

res.render('home', { user: req.user });

})

});


module.exports = router;
