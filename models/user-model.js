const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dataSchema = new Schema({
    data_id: String
    ,data_name_obj: String
    ,obj_store: String
    ,bucket: String
});



const accesS3 = new Schema({
     endPoint : String
    , port : Number
    , secure: Boolean
    , accessKey : String
    , secretKey : String
})

const credGO = new Schema({
        type : String
      , project_id : String
      , private_key_id : String
      , private_key : String
      , client_email : String
      , client_id : String
      ,auth_uri : String
      ,token_uri : String
      ,auth_provider_x509_cert_url : String
      ,client_x509_cert_url : String
      })

const accesGO = new Schema({
     projectId: String
     , location: String
     , storageClass : String
     , cred : credGO
   })


const objectStoragSchema = new Schema({
    name: String
    , typ: String
    , expiresinS : Number
    , accesS3 : accesS3
    , accesGO : accesGO
  })

const userSchema = new Schema({
    username: String
    ,googleId: String
    ,token: String
    ,objectStorag: [objectStoragSchema]
    , data :[dataSchema]

});



const User = mongoose.model('user', userSchema);

module.exports = User;
