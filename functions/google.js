const Storage = require('@google-cloud/storage');

class google{

  upload(data, bucket, location, storageClass , storageNum, user){
    return new Promise(function(resolve,reject){
      // Creates a client
      const stor = new Storage({
        projectId: user.objectStorag[storageNum].accesGO.projectId,
        credentials: user.objectStorag[storageNum].accesGO.cred
      });
      const bucketName = bucket;

      stor.getBuckets()
        .then(results => {
          const buckets = results[0];
            for(var i = 0;i<buckets.length ;i++){
                if (buckets[i].name == bucketName){
                  console.log("bucket exists");

                  googleupload(stor, bucketName, data)
                    .then( result =>{
                      resolve(result);
                    })
                    .catch(err =>{
                      reject(err);
                    });
                    break;
                }
                else if (i == buckets.length-1) {
                  stor.createBucket(bucketName, {
                    location: location,
                    storageClass: storageClass,
                  })
                    .then(() => {
                      console.log(`Bucket ${bucketName} created.`);
                        googleupload(stor, bucketName, data)
                          .then( result =>{
                            resolve(result);
                          })
                          .catch(err =>{
                            reject(err);
                          });
                    })
                    .catch(err => {
                      console.error('ERROR:', err);
                    });

                }
              }
            })
            .catch(err =>{

              reject('ERROR:'+ err);
            })
    });
  }

  download( fileName, bucket, expiresinS, storageNum, user ){
    return new Promise(function(resolve,reject){
      const stor = new Storage({
        projectId: user.objectStorag[storageNum].accesGO.projectId,
        credentials: user.objectStorag[storageNum].accesGO.cred
      });
      const options = {
        action: 'read',
        expires: Date.now() + expiresinS   ,
      };

      stor
        .bucket(bucket)
        .file(fileName)
        .getSignedUrl(options)
        .then(results => {
          resolve(results[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  getMetadata( fileName, bucket, storageNum, user){
    return new Promise(function(resolve,reject){

      const stor = new Storage({
        projectId: user.objectStorag[storageNum].accesGO.projectId,
        credentials: user.objectStorag[storageNum].accesGO.cred
      });

      stor
        .bucket(bucket)
        .file(fileName)
        .getMetadata()
        .then(results => {
          resolve(results[0]);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  delete( fileName, bucketName, storageNum, user){
    return new Promise(function(resolve,reject){

      const stor = new Storage({
        projectId: user.objectStorag[storageNum].accesGO.projectId,
        credentials: user.objectStorag[storageNum].accesGO.cred
      });

      stor
        .bucket(bucketName)
        .file(fileName)
        .delete()
        .then(() => {


          resolve(`gs://${bucketName}/${fileName} deleted.`);
        })
        .catch(err => {
          reject( err);
        });
    });
  }
}

function googleupload(stor, bucketName, data){
  return new Promise(function(resolve,reject){

    var myBucket = stor.bucket(bucketName);
    var file = myBucket.file(data.path.substr(data.path.lastIndexOf("\\" )+1));

      data
        .pipe(file.createWriteStream())
        .on('error', function(err) {
          reject(err);})
        .on('finish', function() {
          resolve("upload succes");
        });
  })
};

module.exports=new google;
