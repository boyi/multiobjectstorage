const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const passport = require('passport');
const helmet = require('helmet');
const mongoose = require('mongoose');
const formData = require('express-form-data');
const config = require('config');


const passportSetup = require('./config/passport-setup');
const keys = require(config.keypfad);


//routes
const authRoutes = require('./routes/auth-routes');
const profileRoutes = require('./routes/profile-routes');
const multiobjstorRoutes = require('./routes/multiobjstor');
const tokenRoutes = require('./routes/token-routes')
const configStorageRoutes = require('./routes/configStorage-routes')

const multipartyOptions = {
  autoFiles: true
};

var app = express();



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());

// parse a data with connect-multiparty.
app.use(formData.parse(multipartyOptions));
// clear all empty files (size == 0)
app.use(formData.format());
// change file objects to node stream.Readable
app.use(formData.stream());
// union body and files
app.use(formData.union());


// set view engine
app.set('view engine', 'ejs');

// set up session cookies
app.use(cookieSession({
    maxAge: 60 * 60 * 1000, // maximum 1 hour old
    keys: [keys.session.cookieKey]
}));

// initialize passport
app.use(passport.initialize());
app.use(passport.session());


// connect to mongodb

mongoose.connect(keys.mongodb.dbURI, () => {
    console.log('connected to mongodb');
});

// set up routes
app.use('/auth', authRoutes);
app.use('/profile', profileRoutes);
app.use('/multiobjstor', multiobjstorRoutes);
app.use('/token', tokenRoutes);
app.use('/configStorage', configStorageRoutes)

// create home route
app.get('/', (req, res) => {
    res.render('home', { user: req.user });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
