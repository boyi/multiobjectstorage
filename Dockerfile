FROM node
RUN apt-get update
RUN apt-get install -y git
WORKDIR /app
RUN git clone https://gitlab.com/boyi/multiobjectstorage.git
WORKDIR /app/multiobjectstorage
RUN npm install
EXPOSE 3000
CMD ["npm", "start"]
